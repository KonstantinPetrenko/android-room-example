package com.onix.roomdemo.local.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.onix.roomdemo.local.db.entity.ProductList

class Converters {

    @TypeConverter
    fun listToString(productList: List<ProductList>): String{
        return Gson().toJson(productList)
    }
    @TypeConverter
    fun stringToList(string: String): List<ProductList> {
        val listType = object : TypeToken<ProductList>() {}.type
        return Gson().fromJson(string, listType)
    }
}