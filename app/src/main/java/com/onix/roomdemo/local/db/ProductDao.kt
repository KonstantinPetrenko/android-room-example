package com.onix.roomdemo.local.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.onix.roomdemo.local.db.entity.Product
import com.onix.roomdemo.local.db.entity.ProductList

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(vararg product: Product)

    @Query("SELECT * FROM product")
    fun getProductAll(): List<Product>

    @Query("SELECT * FROM product WHERE parent_id = :id")
    fun getProductByParentId(vararg id: Long): List<Product>

    @Query("SELECT * FROM product WHERE id = :id")
    fun getProductById(vararg id: Long): List<Product>

    @Query("DELETE FROM product WHERE id IN (:id)")
    fun deleteProduct(vararg  id: Long)

    @Query("DELETE FROM product")
    fun deleteProducts()


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProductList(vararg listProduct: ProductList): List<Long>

    @Query("SELECT * FROM ProductList")
    fun getProductList(): List<ProductList>

    @Query("SELECT * FROM ProductList WHERE id = :id")
    fun getProductListById(vararg id: Long):List<ProductList>

    @Query("DELETE FROM ProductList WHERE id IN (:id)")
    fun deleteProductListById(vararg  id: Long): Int

    @Query("DELETE FROM ProductList")
    fun deleteProductLists(): Int

}