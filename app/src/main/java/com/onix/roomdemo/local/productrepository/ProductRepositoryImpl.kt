package com.onix.roomdemo.local.productrepository


import android.util.Log
import com.onix.roomdemo.MockProductListFromUI
import com.onix.roomdemo.local.db.ProductDao
import com.onix.roomdemo.local.db.entity.Product
import com.onix.roomdemo.local.db.entity.ProductList
import com.onix.roomdemo.local.db.entity.ProductListData
import com.onix.roomdemo.local.db.mapper.Mapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class ProductRepositoryImpl(
    private val dao: ProductDao,
    private val mockProductListFromUI: MockProductListFromUI,
    private val mapper: Mapper<MockProductListFromUI, ProductListData>
): ProductRepository, ProductListRepository, CoroutineScope {

    lateinit var productListData: ProductListData
    init {
        launch {
           // if (getProductAll().isEmpty() || getProductList().isEmpty()){
                productListData = mapper.map(mockProductListFromUI)
            insertProductList(*productListData.productList.toTypedArray())
                //Log.d("+++", "product size = ${mockProductListFromUI.}")
                Log.d("+++", "productList size = ${productListData.productList.size}")
                Log.d("+++", "product size = ${productListData.product.size}")
            insertProduct(*productListData.product.toTypedArray())
            //}
        }
    }
    //+
    override suspend fun insertProduct(vararg product: Product){
        dao.insertProduct(*product)
    }
    //+
    override suspend fun deleteProduct(id: Long) {
        dao.deleteProduct(id)
    }
    //+
    override suspend fun deleteProducts() {
        dao.deleteProducts()
    }

    //+
    override suspend fun getProductAll(): List<Product> {
       return dao.getProductAll()
    }

    //+
    override suspend fun getProductByParentId(vararg id: Long): List<Product> {
     return dao.getProductByParentId(*id)
    }

    override suspend fun getProductById(vararg id: Long): List<Product> {
        return dao.getProductById(*id)
    }

    //+
    override suspend fun insertProductList(vararg productList: ProductList): List<Long> {
       return dao.insertProductList(*productList)
    }

    //+
    override suspend fun deleteProductsLists() {
        dao.deleteProductLists()
    }

    //+
    override suspend fun deleteProductsLists(vararg id: Long) {
        dao.deleteProductListById(*id)
    }

    //+
    override suspend fun getProductList()
            = dao.getProductList()

    //+
    override suspend fun getProductListById(vararg id: Long)
            = dao.getProductListById(*id)


    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + Job()
}