package com.onix.roomdemo.local.productrepository

import androidx.lifecycle.LiveData
import com.onix.roomdemo.local.db.entity.ProductList

interface ProductListRepository {

    suspend fun insertProductList(vararg productList: ProductList): List<Long>

    suspend fun deleteProductsLists()
    suspend fun deleteProductsLists(vararg id: Long)

    suspend fun getProductList(): List<ProductList>
    suspend fun getProductListById(vararg id: Long): List<ProductList>
}