package com.onix.roomdemo.local.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ProductList(
    @PrimaryKey
    val id: Long,
    val name: String)