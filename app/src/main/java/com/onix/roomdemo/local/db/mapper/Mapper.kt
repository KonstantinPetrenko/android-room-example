package com.onix.roomdemo.local.db.mapper

interface Mapper<I,O> {
    fun map(input: I):O
}