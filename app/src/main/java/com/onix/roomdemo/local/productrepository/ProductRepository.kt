package com.onix.roomdemo.local.productrepository

import com.onix.roomdemo.local.db.entity.Product

interface ProductRepository {

    suspend fun insertProduct(vararg product: Product)
    suspend fun deleteProduct(id: Long)
    suspend fun deleteProducts()

    suspend fun getProductAll():List<Product>
    suspend fun getProductByParentId(vararg id: Long): List<Product>
    suspend fun getProductById(vararg id: Long): List<Product>
}