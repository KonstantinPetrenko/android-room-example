package com.onix.roomdemo.local.db.entity

data class ProductListData(val productList: List<ProductList>, val product: List<Product>)