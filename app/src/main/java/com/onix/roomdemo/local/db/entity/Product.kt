package com.onix.roomdemo.local.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey

@Entity(tableName = "product",
      foreignKeys = [
          ForeignKey(entity = ProductList::class,
              parentColumns = ["id"],
              childColumns = ["parent_id"],
              onDelete = CASCADE)])
data class Product(
    @PrimaryKey
    val id: Long,
    val name: String,
    @ColumnInfo(name = "parent_id")
    val parent_id: Long)