package com.onix.roomdemo.local.db.mapper

import com.onix.roomdemo.MockProductList
import com.onix.roomdemo.MockProductListFromUI
import com.onix.roomdemo.local.db.entity.Product
import com.onix.roomdemo.local.db.entity.ProductList
import com.onix.roomdemo.local.db.entity.ProductListData

class DatabaseMapper: Mapper<MockProductListFromUI, ProductListData> {
    override fun map(input: MockProductListFromUI): ProductListData {
        return ProductListData(
            productList = extractProductLists(input.mockProductListFromUI),
            product = extractProduct(input.mockProductListFromUI))
    }

    private fun extractProductLists(mockProductListFromUI: Array<MockProductList>): List<ProductList> {
       return mockProductListFromUI.map {
            ProductList(id = it.id, name = it.name)
        }
    }

    private fun extractProduct(mockProductListFromUI: Array<MockProductList>): ArrayList<Product> {
        val productAll = ArrayList<Product>()
        mockProductListFromUI.map {
            it.mockProduct.forEach { product ->
                productAll.add(Product(id = product.id, name = product.name, parent_id = product.parent_id))
            }
        }
        return productAll
    }


}