package com.onix.roomdemo.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.onix.roomdemo.local.db.entity.ProductList
import com.onix.roomdemo.local.db.entity.Product

@Database(entities = [ProductList::class, Product::class], version = 2)
abstract class AppDataBase: RoomDatabase(){
    abstract fun dao(): ProductDao
}