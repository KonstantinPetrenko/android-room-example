package com.onix.roomdemo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.onix.roomdemo.R
import com.onix.roomdemo.local.db.entity.Product
import kotlinx.android.synthetic.main.item.view.*

class AdapterProduct(
    val product: List<Product>
): RecyclerView.Adapter<AdapterProduct.ProductHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder {
        return ProductHolder(
            LayoutInflater.from(parent.context)
            .inflate(R.layout.item, parent, false))
    }

    override fun getItemCount() = product.size

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        holder.itemView.idProduct.text = product[position].id.toString()
        holder.itemView.nameProduct.text = product[position].name
    }

    inner class ProductHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}