package com.onix.roomdemo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.onix.roomdemo.OnItemListener
import com.onix.roomdemo.R
import com.onix.roomdemo.local.db.entity.ProductList
import kotlinx.android.synthetic.main.item.view.*

class AdapterProductList(
    val productList: List<ProductList>,
    val onItemListener: OnItemListener
) : RecyclerView.Adapter<AdapterProductList.ProductHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder {
        return ProductHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item, parent, false))
    }

    override fun getItemCount() = productList.size

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        holder.itemView.idProduct.text = productList[position].id.toString()
        holder.itemView.nameProduct.text = productList[position].name

        holder.itemView.container.setOnClickListener {
            onItemListener.onClick(productList[position].id)
        }
    }

    inner class ProductHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}

