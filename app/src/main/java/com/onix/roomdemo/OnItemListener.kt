package com.onix.roomdemo

interface OnItemListener {

    fun onClick(id: Long)
}