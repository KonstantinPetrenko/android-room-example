package com.onix.roomdemo

data class MockProductList(
    val id: Long,
    val name: String,
    val mockProduct: Array<MockProduct>
)