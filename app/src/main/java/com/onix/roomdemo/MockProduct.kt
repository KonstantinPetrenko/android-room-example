package com.onix.roomdemo

data class MockProduct(val id: Long,
                       val name: String,
                       val parent_id: Long)