package com.onix.roomdemo

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.onix.roomdemo.adapters.AdapterProduct
import com.onix.roomdemo.adapters.AdapterProductList
import com.onix.roomdemo.local.db.AppDataBase
import com.onix.roomdemo.local.db.entity.Product
import com.onix.roomdemo.local.db.entity.ProductList
import com.onix.roomdemo.local.db.mapper.DatabaseMapper
import com.onix.roomdemo.local.productrepository.ProductRepositoryImpl
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


class MainActivity : AppCompatActivity(), CoroutineScope, OnItemListener {

    private lateinit var repo: ProductRepositoryImpl
    private lateinit var productList: ArrayList<ProductList>
    private var product: ArrayList<Product>? = null
    val mockModel = MockProductListFromUI(mockDataProductList())
    private var mockId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val dbRoom = Room.databaseBuilder(
            applicationContext,
            AppDataBase::class.java, "database"
        )
            .allowMainThreadQueries()
            .build()

        repo = ProductRepositoryImpl(dbRoom.dao(), mockModel, DatabaseMapper())
    }

    override fun onResume() {
        super.onResume()
        launch {
            productList = repo.getProductList() as ArrayList<ProductList>
                recycler.layoutManager = LinearLayoutManager(this@MainActivity)
                recycler.adapter = AdapterProductList(
                    productList,
                    this@MainActivity as OnItemListener
                )
            product = repo.getProductAll() as ArrayList<Product>
        }


        deleteProductId.setOnClickListener{
            product?.let {
                CoroutineScope(Dispatchers.Main).launch {
                    repo.deleteProduct(it.last().id)
                }
            }
        }

        addProductListFab.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {repo.insertProductList(ProductList(
                id = (productList.size + 1).toLong(),
                name = "name${productList.size + 1}"
            ))}


        }

        deleteProductListFab.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                repo.deleteProductsLists(productList.last().id)
            }
        }

        deleteProductsAllFab.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                repo.deleteProducts()
            }
        }

        deleteListProductAllFab.setOnClickListener {
           launch {
                repo.deleteProductsLists()
            }
        }

        getProductListByIdFab.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                Toast.makeText(applicationContext, "name = ${repo.getProductListById(5).first().name}", Toast.LENGTH_LONG).show()
            }
        }

        getProductByIdFab.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                Toast.makeText(applicationContext, "name = ${repo.getProductById(5).first().name}", Toast.LENGTH_LONG).show()
            }
        }
    }
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + Job()

    override fun onClick(id: Long) {
        CoroutineScope(Dispatchers.Main).launch {
            recycler.adapter = AdapterProduct(
                repo.getProductByParentId(id)
            )
        }
    }

    private fun mockDataProductList(): Array<MockProductList> {
        val list = ArrayList<MockProductList>()
        for (i in 0..10){
            list.add(
                MockProductList(
                    id = i.toLong(),
                    name = "name$i",
                    mockProduct = mockDataProduct(i)
                )
            )
        }
        return list.toTypedArray()
    }

    private fun mockDataProduct(parentId: Int): Array<MockProduct>{
        val list = ArrayList<MockProduct>()
        for (i in 0..10){
            val id = mockId++
            Log.d("+++", "id product = $id")
            list.add(
                MockProduct(
                    id = id.toLong(),
                    name = "name${i} parentId$parentId",
                    parent_id = parentId.toLong()
                )
            )
        }

        return list.toTypedArray()
    }
}





